kubectl apply -f https://raw.githubusercontent.com/open-policy-agent/gatekeeper/release-3.1/deploy/gatekeeper.yaml
kubectl apply -f https://raw.githubusercontent.com/Azure/azure-policy/master/built-in-references/Kubernetes/load-balancer-no-public-ips/template.yaml
kubectl apply -f - <<EOF
apiVersion: constraints.gatekeeper.sh/v1beta1
kind: K8sAzureLoadBalancerNoPublicIPs
metadata:
  name: load-balancer-no-public-ips
spec:
  match:
    excludedNamespaces: ["kube-system","ingress","istio-system"]
    kinds:
      - apiGroups: [""]
        kinds: ["Service"]
EOF

kubectl create deploy nginx --image nginx --port 80
kubectl expose deploy nginx --type LoadBalancer --port 80 #fails
kubectl expose deploy nginx --type LoadBalancer --port 80 --overrides='{ "apiVersion": "v1" , "kind": "Service" , "metadata": { "annotations": { "service.beta.kubernetes.io/azure-load-balancer-internal": "true" } } }'